include(../QSC2ReplayAnalyser.pri)

QT += testlib
QT -= gui

TEMPLATE = app
CONFIG   += console
CONFIG   -= app_bundle

INCLUDEPATH += $$QSC2REPLAYANALYSER_INCLUDE

LIBS += -L$$QSC2REPLAYANALYSER_BIN
LIBS += -lQSC2ReplayParser


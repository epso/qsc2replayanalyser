#ifndef TESTQDATASTREAM_HPP
#define TESTQDATASTREAM_HPP

#include <QObject>

class TestQDataStream : public QObject
{
    Q_OBJECT

public:
    TestQDataStream(QObject *parent =0);

private slots:
    void tst1();

};

#endif // TESTQDATASTREAM_HPP

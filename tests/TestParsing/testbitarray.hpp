#ifndef TESTBITARRAY_HPP
#define TESTBITARRAY_HPP

#include <QObject>

class TestBitArray : public QObject
{
    Q_OBJECT

public:
    TestBitArray(QObject *parent =0);


private slots:
    void tstToString1();
    void tstToString2();
    void tstToString3();
    void tstToHex1();
    void tstToHex2();
    void tstToHex3();
    void tstTakeFromBack();
    void tstTakeFromFront();
};

#endif // TESTBITARRAY_HPP

#include "testbitarray.hpp"
#include "testgameeventparser.hpp"
#include "testqdatastream.hpp"
#include "teststreamreader.hpp"
#include "testgameeventparserwholereplay.hpp"

#include <QTest>

int main(int argc, char *argv[])
{
    TestBitArray test1;
    QTest::qExec(&test1, argc, argv);

    TestGameEventParser test2;
    QTest::qExec(&test2, argc, argv);

    TestQDataStream test3;
    QTest::qExec(&test3, argc, argv);

    TestStreamReader test4;
    QTest::qExec(&test4, argc, argv);

    TestGameEventParserWholeReplay test5;
    QTest::qExec(&test5, argc, argv);

    return 0;
}

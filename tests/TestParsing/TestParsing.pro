include(../tests.pri)

CONFIG += testcase

HEADERS += \
    testqdatastream.hpp \
    teststreamreader.hpp \
    testgameeventparser.hpp \
    testbitarray.hpp \
    testgameeventparserwholereplay.hpp

SOURCES += \
    testqdatastream.cpp \
    teststreamreader.cpp \
    testgameeventparser.cpp \
    testbitarray.cpp \
    main.cpp \
    testgameeventparserwholereplay.cpp

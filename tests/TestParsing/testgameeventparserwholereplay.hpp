#ifndef TESTGAMEEVENTPARSERWHOLEREPLAY_HPP
#define TESTGAMEEVENTPARSERWHOLEREPLAY_HPP

#include <QObject>

class TestGameEventParserWholeReplay : public QObject
{
    Q_OBJECT
public:
    explicit TestGameEventParserWholeReplay(QObject *parent = 0);
    
private slots:
    void tstFile_Bassins_luxuriants();
    
};

#endif // TESTGAMEEVENTPARSERWHOLEREPLAY_HPP

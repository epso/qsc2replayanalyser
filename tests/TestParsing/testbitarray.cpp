#include "testbitarray.hpp"

#include <utils/BitArray>

#include <QTest>

TestBitArray::TestBitArray(QObject *parent) : QObject(parent)
{}

void TestBitArray::tstToString1()
{
    quint8 i = 5;
    BitArray b(i);
    QCOMPARE(b.toString(), QString("00000101"));
}

void TestBitArray::tstToString2()
{
    quint8 i = 10;
    BitArray b(i);
    QCOMPARE(b.toString(), QString("00001010"));
}

void TestBitArray::tstToString3()
{
    quint8 i = 255;
    BitArray b(i);
    QCOMPARE(b.toString(), QString("11111111"));
}

void TestBitArray::tstToHex1()
{
    quint8 i = 5;
    BitArray b(i);
    QCOMPARE(b.toHex(), QString("05"));
}

void TestBitArray::tstToHex2()
{
    quint8 i = 10;
    BitArray b(i);
    QCOMPARE(b.toHex(), QString("0A"));
}

void TestBitArray::tstToHex3()
{
    quint8 i = 255;
    BitArray b(i);
    QCOMPARE(b.toHex(), QString("FF"));
}

void TestBitArray::tstTakeFromBack()
{
    quint16 i = 0xA2B7;
    BitArray b(i);
    QCOMPARE(b.toString(), QString("1010001010110111"));
    BitArray b2 = b.takeFromBack(5);
    QCOMPARE(b.toString(), QString("10100010101"));
    QCOMPARE(b2.toString(), QString("10111"));
}



void TestBitArray::tstTakeFromFront()
{
    quint16 i = 0xA2B7;
    BitArray b(i);
    QCOMPARE(b.toString(), QString("1010001010110111"));
    BitArray b2 = b.takeFromFront(5);
    QCOMPARE(b.size(), (int)4*4-5);
    QCOMPARE(b2.size(), (int)5);
    QCOMPARE(b.toString(), QString("01010110111"));
    QCOMPARE(b2.toString(), QString("10100"));
}

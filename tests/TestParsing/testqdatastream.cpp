#include "testqdatastream.hpp"

#include <MPQFile>

#include <QTest>
#include <QDataStream>

TestQDataStream::TestQDataStream(QObject *parent) : QObject(parent)
{}

void TestQDataStream::tst1()
{
    int size = 10;
    unsigned char* data = new unsigned char[size];
    for(unsigned char i = 0; i < size; ++i)
        data[i] = 2*i+1;
    MPQFile file("", data, size);

    QByteArray arr;
    QDataStream flux(&arr, QIODevice::ReadWrite);
    bool res = file.toDataStream(flux);
    QVERIFY(res);
    flux.device()->seek(0);

    quint8 in;
    for(unsigned char i = 0; i < size; ++i)
    {
        flux >> in;
        QCOMPARE(in, (quint8)(2*i+1));
    }
}

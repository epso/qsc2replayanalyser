#include "teststreamreader.hpp"

#include <StreamReader>
#include <MPQFile>
#include <utils/BitArray>

#include <QTest>

TestStreamReader::TestStreamReader(QObject *parent) : QObject(parent)
{}

void TestStreamReader::tst1()
{
    int size = 4;
    unsigned char* data = new unsigned char[size];
    data[0] = 0xAB;
    data[1] = 0xF9;
    data[2] = 0x57;
    data[3] = 0x03;
    MPQFile file("", data, size);

    //QByteArray arr;
    StreamReader flux; //(&arr, QIODevice::ReadWrite);
    bool res = file.toStreamReader(flux);
    QVERIFY(res);
    flux.seek(0);

    quint8 in;
    BitArray b = flux.startBitShift(3);
    QCOMPARE(b.size(), 3);
    QCOMPARE(b.toChar(), (quint8)3);

    flux >> in;
    QCOMPARE(in, (quint8)0xA9);
    flux >> in;
    QCOMPARE(in, (quint8)0xFF);
    flux >> in;
    QCOMPARE(in, (quint8)0x53);

    QVERIFY(flux.endBitShift());
}

void TestStreamReader::tstBitShift1()
{
    int size = 10;
    unsigned char* data = new unsigned char[size];
    for(unsigned char i = 0; i < size; ++i)
        data[i] = 2*i+1;
    MPQFile file("", data, size);

    StreamReader flux;
    bool res = file.toStreamReader(flux);
    QVERIFY(res);
    flux.seek(0);

    quint8 in;
    for(unsigned char i = 0; i < size; ++i)
    {
        flux >> in;
        QCOMPARE(in, (quint8)(2*i+1));
    }
}

void TestStreamReader::tst3BytesRead0BitShift()
{
    int size = 3;
    unsigned char* data = new unsigned char[size];
    data[0] = 0xF0;
    data[1] = 0xF5;
    data[2] = 0x50;
    MPQFile file("", data, size);

    //QByteArray arr;
    StreamReader flux; //(&arr, QIODevice::ReadWrite);
    bool res = file.toStreamReader(flux);
    QVERIFY(res);
    flux.seek(0);

    BitArray b = flux.readNBytes(3);
    QCOMPARE(b.size(), 3*8);
    QCOMPARE(b.toLong(), (quint64)0x00F0F550);
}

void TestStreamReader::tst3BytesRead1BitShift()
{
    int size = 4;
    unsigned char* data = new unsigned char[size];
    data[0] = 0xF1;
    data[1] = 0x7A;
    data[2] = 0xA8;
    data[3] = 0x00;
    MPQFile file("", data, size);

    //QByteArray arr;
    StreamReader flux; //(&arr, QIODevice::ReadWrite);
    bool res = file.toStreamReader(flux);
    QVERIFY(res);
    flux.seek(0);

    BitArray b = flux.startBitShift(1);
    QCOMPARE(b.size(), 1);
    QCOMPARE(b.toChar(), (quint8)1);

    b = flux.readNBytes(3);
    QCOMPARE(b.size(), 3*8);
    QCOMPARE(b.toLong(), (quint64)0x00F0F550);

    res = flux.endBitShift();
    QVERIFY(res);
}

void TestStreamReader::tst3BytesRead3BitShift()
{
    int size = 4;
    unsigned char* data = new unsigned char[size];
    data[0] = 0xF7;
    data[1] = 0x1E;
    data[2] = 0xAA;
    data[3] = 0x00;
    MPQFile file("", data, size);

    //QByteArray arr;
    StreamReader flux; //(&arr, QIODevice::ReadWrite);
    bool res = file.toStreamReader(flux);
    QVERIFY(res);
    flux.seek(0);

    BitArray b = flux.startBitShift(3);
    QCOMPARE(b.size(), 3);
    QCOMPARE(b.toChar(), (quint8)7);

    b = flux.readNBytes(3);
    QCOMPARE(b.size(), 3*8);
    QCOMPARE(b.toLong(), (quint64)0x00F0F550);

    res = flux.endBitShift();
    QVERIFY(res);
}


void TestStreamReader::tstReadTypeID0BitShift()
{
    int size = 3;
    unsigned char* data = new unsigned char[size];
    data[0] = 0x00;
    data[1] = 0x72;
    data[2] = 0x01;
    MPQFile file("", data, size);

    //QByteArray arr;
    StreamReader flux; //(&arr, QIODevice::ReadWrite);
    bool res = file.toStreamReader(flux);
    QVERIFY(res);
    flux.seek(0);

    BitArray b = flux.readTypeID();
    QCOMPARE(b.size(), 3*8);
    QCOMPARE(b.toLong(), (quint64)0x007201);
}

void TestStreamReader::tstReadTypeID1BitShift()
{
    int size = 4;
    unsigned char* data = new unsigned char[size];
    data[0] = 0x01;
    data[1] = 0x39;
    data[2] = 0x00;
    data[3] = 0x01;
    MPQFile file("", data, size);

    //QByteArray arr;
    StreamReader flux; //(&arr, QIODevice::ReadWrite);
    bool res = file.toStreamReader(flux);
    QVERIFY(res);
    flux.seek(0);

    BitArray b = flux.startBitShift(1);
    QCOMPARE(b.size(), 1);
    QCOMPARE(b.toChar(), (quint8)1);

    b = flux.readTypeID();
    QCOMPARE(b.size(), 3*8);
    QCOMPARE(b.toLong(), (quint64)0x007201);

    res = flux.endBitShift();
    QVERIFY(res);
}

void TestStreamReader::tstReadTypeID3BitShift()
{
    int size = 4;
    unsigned char* data = new unsigned char[size];
    data[0] = 0x07;
    data[1] = 0x0E;
    data[2] = 0x02;
    data[3] = 0x01;
    MPQFile file("", data, size);

    //QByteArray arr;
    StreamReader flux; //(&arr, QIODevice::ReadWrite);
    bool res = file.toStreamReader(flux);
    QVERIFY(res);
    flux.seek(0);

    BitArray b = flux.startBitShift(3);
    QCOMPARE(b.size(), 3);
    QCOMPARE(b.toChar(), (quint8)7);

    b = flux.readTypeID();
    QCOMPARE(b.size(), 3*8);
    QCOMPARE(b.toLong(), (quint64)0x007201);

    res = flux.endBitShift();
    QVERIFY(res);
}

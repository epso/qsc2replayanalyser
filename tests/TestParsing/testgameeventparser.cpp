#include "testgameeventparser.hpp"

#include <StreamReader>
#include <MPQFile>
#include <replay/gameevents/GameEvent>
#include <replay/gameevents/GameEvents>
#include <replay/gameevents/Selection>

#include <boost/scoped_ptr.hpp>

#include <QTest>

TestGameEventParser::TestGameEventParser(QObject *parent) : QObject(parent)
{}

void TestGameEventParser::tstSelectionEvent1()
{
    QByteArray array(34,0);
    //selection P1
    array[0] = 0x68;
    array[1] = 0x21;
    array[2] = 0xAC;
    array[3] = 0x00;

    array[4] = 0x00; //deselect map
    array[5] = 0x01; //one unit type selected
    array[6] = 0x1C;
    array[7] = 0x00;
    array[8] = 0x05; //probes

    //six units selected
    array[9] = 0x06;
    array[10] = 0x02;
    array[11] = 0x77;
    array[12] = 0x00;
    array[13] = 0x00;
    array[14] = 0x01;
    array[15] = 0x78;
    array[16] = 0x00;
    array[17] = 0x00;
    array[18] = 0x01;
    array[19] = 0x79;
    array[20] = 0x00;
    array[21] = 0x00;
    array[22] = 0x01;
    array[23] = 0x7A;
    array[24] = 0x00;
    array[25] = 0x00;
    array[26] = 0x01;
    array[27] = 0x7B;
    array[28] = 0x00;
    array[29] = 0x00;
    array[30] = 0x01;
    array[31] = 0x7C;
    array[32] = 0x00;
    array[33] = 0x00;


    unsigned char* buffer = new unsigned char[array.size()];
    memcpy(buffer, array.data(), array.size());

    MPQFile file("", buffer, array.size());
    StreamReader flux;
    bool res = file.toStreamReader(flux);
    QVERIFY(res);
    flux.seek(0);

    boost::scoped_ptr<GameEvents>evts(GameEvents::parse(flux));
    QCOMPARE(evts->size(), 1);
    GameEvent* e = evts->at(0);
    QCOMPARE(e->timestamp(), (quint64)0x0000001A);
    QCOMPARE(e->player(), (uint)1);
    Selection* s = dynamic_cast<Selection*>(e);
    Q_ASSERT(s);
    QCOMPARE(s->selections().size(), 1);
    Selection::SelectionPair p = s->selections().at(0);
    QCOMPARE(p.amount(), (quint8)6);
    QCOMPARE(p.id(), (quint32)0x001C0005);
    QCOMPARE(s->idsSelection().size(), 6);

    QCOMPARE(s->idsSelection().at(0), (quint32)0x02770000);
    QCOMPARE(s->idsSelection().at(1), (quint32)0x01780000);
    QCOMPARE(s->idsSelection().at(2), (quint32)0x01790000);
    QCOMPARE(s->idsSelection().at(3), (quint32)0x017A0000);
    QCOMPARE(s->idsSelection().at(4), (quint32)0x017B0000);
    QCOMPARE(s->idsSelection().at(5), (quint32)0x017C0000);
}

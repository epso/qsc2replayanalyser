#include "testgameeventparserwholereplay.hpp"

#include <MPQArchive>
#include <MPQFile>
#include <StreamReader>
#include <replay/gameevents/GameEvent>
#include <replay/gameevents/GameEvents>
#include <replay/gameevents/Selection>

#include <boost/scoped_ptr.hpp>

#include <QTest>

TestGameEventParserWholeReplay::TestGameEventParserWholeReplay(QObject *parent) :
    QObject(parent)
{
}

void TestGameEventParserWholeReplay::tstFile_Bassins_luxuriants()
{
    QString filename = "Bassins luxuriants.SC2Replay";
    MPQArchive::type_ptr archive = MPQArchive::openArchive(filename.toStdString());
    const MPQFile& file = archive->openFile("replay.game.events");

    StreamReader flux;
    bool res = file.toStreamReader(flux);
    QVERIFY(res);
    flux.seek(0);

    boost::scoped_ptr<GameEvents>evts(GameEvents::parse(flux));
    QCOMPARE(evts->size(), 9);

    GameEvent* evt = evts->at(0);
    QCOMPARE(evt->type(), GameEvent::PLAYER_ENTER);

    QCOMPARE(evts->at(1)->type(), GameEvent::GAME_START);
    QCOMPARE(evts->at(2)->type(), GameEvent::SCREEN_MOVEMENT);

    evt = evts->at(3);
    QCOMPARE(evt->type(), GameEvent::SELECTION);
    Selection* s = dynamic_cast<Selection*>(evt);
    assert(s);
    QCOMPARE(s->selections().size(), 1);
    QCOMPARE(s->idsSelection().size(), 1);

    QCOMPARE(evts->at(4)->type(), GameEvent::ABILITY);

    evt = evts->at(5);
    QCOMPARE(evt->type(), GameEvent::SELECTION);
    s = dynamic_cast<Selection*>(evt);
    assert(s);
    QCOMPARE(s->selections().size(), 1);
    QCOMPARE(s->idsSelection().size(), 6);

    QCOMPARE(evts->at(6)->type(), GameEvent::ABILITY);
    QCOMPARE(evts->at(7)->type(), GameEvent::TYPE_TWO);
    QCOMPARE(evts->at(8)->type(), GameEvent::PLAYER_LEFT);
}

#ifndef TESTSTREAMREADER_HPP
#define TESTSTREAMREADER_HPP

#include <QObject>

class TestStreamReader : public QObject
{
    Q_OBJECT

public:
    TestStreamReader(QObject *parent =0);


private slots:
    void tst1();

    void tstBitShift1();

    void tst3BytesRead0BitShift();
    void tst3BytesRead1BitShift();
    void tst3BytesRead3BitShift();

    void tstReadTypeID0BitShift();
    void tstReadTypeID1BitShift();
    void tstReadTypeID3BitShift();

};

#endif // TESTSTREAMREADER_HPP

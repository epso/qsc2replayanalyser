#include <QCoreApplication>
#include <QStringList>
#include <QFile>

#include <QDebug>
#include <QTimer>

#include <Parser>

int main(int argc, char* argv[])
{
    QCoreApplication app(argc, argv);

    QTextStream qout(stdout); //I connect the stout to my qout textstream
//    qout << "yop\n";

    QStringList args = app.arguments();
    QString input, output;
    for(QStringList::const_iterator i = args.begin(); i != args.end(); ++i)
    {
        qDebug() << *i;
        if(i->startsWith("-"))
        {
            if(*i == "-o")
            {
                ++i;
                if(i == args.end())
                {
                    qout << "No output file name.\n";
                    return false;
                }
                output = *i;
            }
            else if(*i == "--help")
            {

            }
            else
            {
                qout << "Unknown Option: " << *i << '\n';
                return false;
            }
        }
        else
        {
            input = *i;
            if(output.isEmpty()) output = *i;
        }
    }

    if(!QFile::exists(input))
    {
        qout << "Unable to open file: " << input << '\n';
        return false;
    }

    Parser parser;
    parser.loadReplay(input);

    QTimer::singleShot(500, &app, SLOT(quit())); //stop after 5 seconds
    return app.exec(); //and we run the application
}

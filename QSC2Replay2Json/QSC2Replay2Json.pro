QT       += core
QT       -= gui

include(../QSC2ReplayAnalyser.pri)

TARGET = $$SC2REPLAYANALYSER_BIN/QSC2Replay2Json
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

OBJECTS_DIR = $$QSC2REPLAYANALYSER_OBJ
MOC_DIR = $$QSC2REPLAYANALYSER_OBJ

INCLUDEPATH += .
LIBS += -L$$QSC2REPLAYANALYSER_BIN
LIBS += -lStorm -lbz2 -lz -lQSC2ReplayParser

win32 {
    LIBS +=  -lwininet
}

SOURCES += main.cpp

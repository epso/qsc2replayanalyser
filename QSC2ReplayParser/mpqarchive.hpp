#ifndef MPQARCHIVE_HPP
#define MPQARCHIVE_HPP

#include <string>
#include <memory>
#include <map>

class MPQFile;

class MPQArchive
{
public:
    typedef std::auto_ptr<MPQArchive> type_ptr;
    typedef std::map<std::string, MPQFile*> type_map_files;

    ~MPQArchive();

    static type_ptr openArchive(const std::string& filename);

    const MPQFile& openFile(const std::string& filename);

private:
    MPQArchive(const std::string& filename);
    MPQArchive(const MPQArchive& archive);

    void load();
    void close();

    void* m_handle;
    std::string m_filename;
    type_map_files m_files;
};

#endif // MPQARCHIVE_HPP

#-------------------------------------------------
#
# Project created by QtCreator 2012-07-14T20:41:02
#
#-------------------------------------------------

QT       += core gui

include(../QSC2ReplayAnalyser.pri)

TARGET = $$qtLibraryTarget($$TARGET)
TEMPLATE = lib

OBJECTS_DIR = $$QSC2REPLAYANALYSER_OBJ
MOC_DIR = $$QSC2REPLAYANALYSER_OBJ

INCLUDEPATH += .
LIBS += -lStorm -lbz2 -lz

win32 {
    LIBS +=  -lwininet
}

SOURCES += mpqarchive.cpp \
    parser.cpp \
    mpqfile.cpp \
    utils/bitarray.cpp \
    utils/json.cpp \
    replay/player.cpp \
    replay/replay.cpp \
    replay/gameevents/gameevents.cpp \
    replay/gameevents/gameevent.cpp \
    replay/gameevents/gamestart.cpp \
    replay/gameevents/playerenter.cpp \
    replay/parsingerror.cpp \
    replay/entete.cpp \
    streamreader.cpp \
    replay/gameevents/selection.cpp \
    replay/gameevents/ability.cpp \
    replay/gameevents/screenmovement.cpp \
    replay/gameevents/playerleft.cpp \
    replay/gameevents/typetwo.cpp

HEADERS  += \
    mpqarchive.hpp \
    parser.hpp \
    mpqfile.hpp \
    utils/bitarray.hpp \
    utils/json.hpp \
    replay/player.hpp \
    replay/replay.hpp \
    replay/gameevents/gameevents.hpp \
    replay/gameevents/gameevent.hpp \
    replay/gameevents/gameeventfactory.hpp \
    replay/gameevents/gamestart.hpp \
    utils/singleton.hpp \
    utils/factory.hpp \
    utils/defaultfactoryerrorpolicy.hpp \
    utils/traits/parametertrait.hpp \
    replay/gameevents/playerenter.hpp \
    replay/parsingerror.hpp \
    replay/entete.hpp \
    streamreader.hpp \
    replay/gameevents/selection.hpp \
    replay/gameevents/ability.hpp \
    replay/gameevents/screenmovement.hpp \
    replay/gameevents/playerleft.hpp \
    replay/gameevents/typetwo.hpp

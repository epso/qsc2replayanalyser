#ifndef STREAMREADER_HPP
#define STREAMREADER_HPP

#include <QDataStream>
#include <utils/BitArray>

class MPQFile;

class StreamReader
{
public:
    StreamReader();

    bool atEnd() const;

    bool setData(const char* buffer, unsigned int size);

    bool seek(qint64 i);

    BitArray startBitShift(quint8 n);
    bool endBitShift();

    BitArray readNBytes(quint8 n);

    BitArray readTypeID();

    StreamReader& operator>>(quint8& i);
    StreamReader& operator>>(quint16& i);
    StreamReader& operator>>(quint32& i);
    StreamReader& operator>>(quint64& i);

    StreamReader& operator>>(qint8& i);
    StreamReader& operator>>(qint16& i);
    StreamReader& operator>>(qint32& i);
    StreamReader& operator>>(qint64& i);

private:
    QByteArray m_byteArray;
    QDataStream m_flux;
    quint8 m_bitShift;
    BitArray m_reste;
};

#endif // STREAMREADER_HPP

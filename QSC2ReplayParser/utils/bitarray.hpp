#ifndef BITARRAY_H
#define BITARRAY_H

#include <QVector>
#include <QStringList>

class BitArray : public QVector<bool>
{
public:
    BitArray();

    BitArray(quint8 i);

    BitArray(quint16 i);

    BitArray(quint32 i);

    BitArray(quint64 longlong);

    quint64 toLong() const;

    quint8 toChar() const;

    QString toString() const;

    QString toHex() const;

    void push_front(BitArray array);

    void push_back(BitArray array);

    BitArray takeFromBack(quint32 nbBits);

    BitArray takeFromFront(quint32 nbBits);

    static QString toHex(quint8 i);

    static quint8 fromHex(const QString& str);

private:

    static const QStringList chars;
};

#endif // BITARRAY_H

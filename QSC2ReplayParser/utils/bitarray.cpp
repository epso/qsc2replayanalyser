#include "bitarray.hpp"

#include <cmath>
#include <stdexcept>

const QStringList BitArray::chars = QStringList() << "0" << "1" << "2" << "3" << "4" << "5" << "6" << "7" << "8" << "9" << "A" << "B" << "C" << "D" << "E" << "F";

BitArray::BitArray()
{}

BitArray::BitArray(quint8 i)
{
    for(quint8 cpt = 0; cpt < 8; ++cpt)
    {
        QVector<bool>::push_front((i&1)>0);
        i = i>>1;
    }
}

BitArray::BitArray(quint16 i)
{
    for(quint8 cpt = 0; cpt < 16; ++cpt)
    {
        QVector<bool>::push_front((i&1)>0);
        i = i>>1;
    }
}

BitArray::BitArray(quint32 i)
{
    for(quint8 cpt = 0; cpt < 32; ++cpt)
    {
        QVector<bool>::push_front((i&1)>0);
        i = i>>1;
    }
}

BitArray::BitArray(quint64 longlong)
{
    push_back(BitArray((quint8)(longlong&255)));
    longlong = longlong >> 8;
    push_front(BitArray((quint8)(longlong&255)));
    longlong = longlong >> 8;
    push_front(BitArray((quint8)(longlong&255)));
    longlong = longlong >> 8;
    push_front(BitArray((quint8)(longlong&255)));
    longlong = longlong >> 8;
}

quint8 BitArray::toChar() const
{
    quint8 res = 0;
    int taille = size();
    if((unsigned int)taille > sizeof(quint8)*8) throw std::logic_error(("Le BitArray est trop long pour etre convertit en quint8: " + QString::number(taille) + " / " + QString::number(sizeof(quint8)*8)).toStdString());
    for(int i = 0; i < taille; ++i)
    {
        if(at(taille - i - 1)) {
            res += pow(2,i);
        }
    }
    return res;
}

quint64 BitArray::toLong() const
{
    quint64 res = 0;
    int taille = size();
    //if((unsigned int)taille > sizeof(quint64)*8) throw std::logic_error(("Le BitArray est trop long pour etre convertit en quint64: " + QString::number(taille) + " / " + QString::number(sizeof(quint64)*8)).toStdString());
    for(int i = 0; i < taille; ++i)
    {
        if(at(taille - i - 1)) {
            res += pow(2,i);
        }
    }
    return res;
}

QString BitArray::toString() const
{
    QString res;
    for(int i = 0; i < size(); ++i)
    {
        if(at(i)) res += "1";
        else res += "0";
    }
    return res;
}

QString BitArray::toHex() const
{
    BitArray b(*this);
    QString res;
    while(b.size() >= 8)
    {
        BitArray tmp = b.takeFromBack(8);
        res.push_front(toHex(tmp.toChar()));

    }
    if(!b.empty())res.push_front(toHex(b.toChar()));
    return res;
}

void BitArray::push_front(BitArray array)
{
    bool b;
    while(!array.isEmpty())
    {
        b = array.back();
        array.pop_back();
        QVector<bool>::push_front(b);
    }
}

void BitArray::push_back(BitArray array)
{
    bool b;
    while(!array.isEmpty())
    {
        b = array.front();
        array.pop_front();
        QVector<bool>::push_back(b);
    }
}

BitArray BitArray::takeFromBack(quint32 nbBits)
{
    BitArray res;
    for(quint32 cpt = 0; cpt < nbBits; ++cpt)
    {
        res.QVector<bool>::push_front(back());
        pop_back();
    }
    return res;
}

BitArray BitArray::takeFromFront(quint32 nbBits)
{
    BitArray res;
    for(quint32 cpt = 0; cpt < nbBits; ++cpt)
    {
        res.QVector<bool>::push_back(front());
        pop_front();
    }
    return res;
}

QString BitArray::toHex(quint8 i)
{
    quint8 d = i & 0xF0;
    d /= 16;
    quint8 u = i & 0xF;
    return chars[d] + chars[u];
}

quint8 BitArray::fromHex(const QString& str)
{
    if(str.size() != 2) throw std::invalid_argument(("string too long to be parsed as a byte: " + str).toStdString());
    quint8 res;
    res = chars.indexOf(str.at(0))*16 + chars.indexOf(str.at(1));
    return res;
}


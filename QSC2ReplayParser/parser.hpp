#ifndef PARSER_HPP
#define PARSER_HPP

#include <replay/EnTete>

#include <QObject>

class MPQFile;
class Replay;

class Parser : public QObject
{
    Q_OBJECT
public:
    explicit Parser(QObject *parent = 0);

    void loadReplay(const QString& filename);

    void parseHeader(const QString& file);
    void parseDetailsFile(const MPQFile& detailsFile);
    void parseGameEventsFile(const MPQFile& gameEventsFile);

private:
    Replay* m_replay;
    EnTete m_enTete;

    static quint8 start[4];

    QVariant parse(QDataStream& flux) const;
    static qint8 parseSingleByte(quint8 i);
    
signals:
    void erreur(QString);
    void message(QString);

public slots:
    
};

#endif // PARSER_HPP

#include "mpqarchive.hpp"
#include "mpqfile.hpp"

#include <StormLib/StormLib.h>

#include <stdexcept>
#include <sstream>

MPQArchive::MPQArchive(const std::string& filename): m_handle(0), m_filename(filename)
{
    load();
}

MPQArchive::MPQArchive(const MPQArchive& archive): m_handle(0), m_filename(archive.m_filename)
{
    load();
}

MPQArchive::~MPQArchive()
{
    close();
}

MPQArchive::type_ptr MPQArchive::openArchive(const std::string& filename)
{
    return type_ptr(new MPQArchive(filename));
}

const MPQFile& MPQArchive::openFile(const std::string& filename)
{
    type_map_files::const_iterator i = m_files.find(filename);
    if(i != m_files.end()) return *(i->second);

    if(!SFileHasFile(m_handle, filename.c_str()))
        throw std::invalid_argument("L'archive ne contient pas le fichier suivant: " + filename);

    HANDLE   file   = 0;
    DWORD    size   = 0;
    uint8_t* buffer = 0;
    if(!SFileOpenFileEx( m_handle, filename.c_str(), 0, &file))
    {
        std::ostringstream oss;
        oss << "Impossible d'ouvrir le fichier (1) " << filename << " : " << GetLastError();
        throw std::logic_error(oss.str());
    }
    size = SFileGetFileSize(file, NULL );

    buffer = new uint8_t[size];

    SFileReadFile( file, buffer, size, &size, NULL);
    SFileCloseFile( file );

    m_files[filename] = new MPQFile(filename, buffer, size);
    return *m_files[filename];
}

void MPQArchive::load()
{
    bool res = SFileOpenArchive(m_filename.c_str(), 0, 0, &m_handle);
    if(!res)
    {
        std::ostringstream oss;
        oss << "Impossible d'ouvrir le fichier (2) " << m_filename << " : " << GetLastError();
        throw std::logic_error(oss.str());
    }
}

void MPQArchive::close()
{
    for(type_map_files::iterator i = m_files.begin(); i != m_files.end(); ++i)
        delete i->second;
    m_files.clear();
    if(m_handle) SFileCloseArchive(m_handle);
}

#include "streamreader.hpp"
#include "mpqfile.hpp"

#include <cassert>
#include <stdexcept>

StreamReader::StreamReader(): m_byteArray(), m_flux(&m_byteArray, QIODevice::ReadWrite),m_bitShift(0), m_reste()
{}

bool StreamReader::atEnd() const
{
    return m_flux.atEnd();
}

bool StreamReader::setData(const char* buffer, unsigned int size)
{
    return m_flux.writeRawData(buffer, size) == (int)size;
}

bool StreamReader::seek(qint64 i) {
    return m_flux.device()->seek(i);
}

BitArray StreamReader::startBitShift(quint8 n)
{
    BitArray b;
    quint8 i;
    while(n >=8)
    {
        m_flux >> i;
        b.push_back(BitArray(i));
    }
    if(n > 0)
    {
        m_bitShift = n;
        m_flux >> i;
        BitArray b2(i);
        b.push_back(b2.takeFromBack(m_bitShift));
        m_reste = b2;
    }
    return b;
}

bool StreamReader::endBitShift()
{
    if(m_bitShift == 0) return true;
    bool res = m_reste.size() == 8-m_bitShift && m_reste.toChar() == 0;
    m_reste.clear();
    m_bitShift = 0;
    return res;
}

BitArray StreamReader::readNBytes(quint8 n)
{
    BitArray res;
    quint8 i;
    if(m_bitShift == 0)
    {
        for(quint8 j = 0; j < n; j++)
        {
            m_flux >> i;
            res.push_back(i);
        }
        return res;
    }
    if(n == 0) return res;
    for(quint8 j = 0; j < n-1; j++)
    {
        BitArray b = m_reste;
        m_flux >> i;
        m_reste = BitArray(i);
        b.push_back(m_reste.takeFromFront(m_bitShift));
        res.push_back(b);
    }
    BitArray b = m_reste;
    m_flux >> i;
    m_reste = BitArray(i);
    b.push_back(m_reste.takeFromBack(m_bitShift));
    res.push_back(b);
    return res;
}

BitArray StreamReader::readTypeID()
{
    if(m_bitShift == 0) return readNBytes(3);

    quint8 i;
    BitArray res = m_reste;
    m_flux >> i;
    m_reste = BitArray(i);
    res.push_back(m_reste.takeFromFront(m_bitShift));

    res.push_back(m_reste);
    m_flux >> i;
    m_reste = BitArray(i);
    res.push_back(m_reste.takeFromBack(m_bitShift));

    res.push_back(m_reste);
    m_flux >> i;
    m_reste = BitArray(i);
    res.push_back(m_reste.takeFromBack(m_bitShift));

    return res;
}

StreamReader& StreamReader::operator>>(quint8& i)
{
    if(m_bitShift == 0)
    {
        m_flux >> i;
        return *this;
    }
    quint8 n;
    m_flux >> n;
    BitArray b(n);
    m_reste.push_back(b.takeFromBack(m_bitShift));
    assert(m_reste.size() == 8);
    n = m_reste.toChar();
    m_reste = b;
    assert(m_reste.size() == 8 - m_bitShift);
    i = n;
    return *this;
}

StreamReader& StreamReader::operator>>(quint16& i)
{
    if(m_bitShift == 0)
    {
        m_flux >> i;
        return *this;
    }
    throw std::logic_error("Reading 16 bits while in bitshift mode not yet implemented");
}

StreamReader& StreamReader::operator>>(quint32& i)
{
    if(m_bitShift == 0)
    {
        m_flux >> i;
        return *this;
    }
    throw std::logic_error("Reading 32 bits while in bitshift mode not yet implemented");
}

StreamReader& StreamReader::operator>>(quint64& i)
{
    if(m_bitShift == 0)
    {
        m_flux >> i;
        return *this;
    }
    throw std::logic_error("Reading 64 bits while in bitshift mode not yet implemented");
}

StreamReader& StreamReader::operator>>(qint8& i)
{
    if(m_bitShift == 0)
    {
        m_flux >> i;
        return *this;
    }
    throw std::logic_error("Reading 8 bits while in bitshift mode not yet implemented");
}

StreamReader& StreamReader::operator>>(qint16& i)
{
    if(m_bitShift == 0)
    {
        m_flux >> i;
        return *this;
    }
    throw std::logic_error("Reading 16 bits while in bitshift mode not yet implemented");
}

StreamReader& StreamReader::operator>>(qint32& i)
{
    if(m_bitShift == 0)
    {
        m_flux >> i;
        return *this;
    }
    throw std::logic_error("Reading 32 bits while in bitshift mode not yet implemented");
}

StreamReader& StreamReader::operator>>(qint64& i)
{
    if(m_bitShift == 0)
    {
        m_flux >> i;
        return *this;
    }
    throw std::logic_error("Reading 64 bits while in bitshift mode not yet implemented");
}

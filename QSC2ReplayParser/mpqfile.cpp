#include "mpqfile.hpp"
#include "streamreader.hpp"

#include <QDataStream>

MPQFile::MPQFile(const std::string &filename, unsigned char *buffer, unsigned int size):
    m_filename(filename), m_buffer(buffer), m_size(size)
{}

MPQFile::~MPQFile()
{
    if(m_buffer) delete[] m_buffer;
}

const std::string& MPQFile::filename() const
{
    return m_filename;
}

unsigned int MPQFile::size() const
{
    return m_size;
}

const unsigned char* MPQFile::buffer() const
{
    return m_buffer;
}

bool MPQFile::toDataStream(QDataStream& stream) const
{
    return stream.writeRawData((const char*)m_buffer, m_size) == m_size;
}

bool MPQFile::toStreamReader(StreamReader& stream) const
{
    return stream.setData((const char*)m_buffer, m_size);
}

MPQFile::MPQFile(const MPQFile&)
{
    throw "";
}

#include "typetwo.hpp"
#include "gameeventfactory.hpp"

TypeTwo::TypeTwo(const QVariantMap& data)
{
    init(data);
    QVariantMap::const_iterator i = data.find("data");
    Q_ASSERT(i != data.end());
    m_data = i.value().toString();
}

QString TypeTwo::toString() const
{
    return "type two event: " + m_data;
}

TypeTwo::TYPES TypeTwo::type() const
{
    return sType();
}

namespace {
GameEvent* createTypeTwo(const QVariantMap& data)
{
    return new TypeTwo(data);
}
}

const bool TypeTwo::s_registered = GameEventFactory::instance().registerProduct(TypeTwo::sType(), &createTypeTwo);

#ifndef GAMEEVENT_HPP
#define GAMEEVENT_HPP

#include <QVariantMap>

class GameEvent
{
public:
    GameEvent();

    enum TYPES {GAME_START, PLAYER_ENTER, SELECTION, ABILITY, SCREEN_MOVEMENT, PLAYER_LEFT, TYPE_TWO};

    quint64 timestamp() const;
    uint player() const;

    virtual QString toString() const =0;

    virtual TYPES type() const =0;

protected:
    void init(const QVariantMap& data);

private:
    quint64 m_timestamp;
    uint m_player;
};

#endif // GAMEEVENT_HPP

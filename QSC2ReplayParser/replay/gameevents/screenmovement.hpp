#ifndef SCREENMOVEMENT_HPP
#define SCREENMOVEMENT_HPP

#include "gameevent.hpp"

class ScreenMovement : public GameEvent
{
public:
    ScreenMovement(const QVariantMap& data);

    virtual QString toString() const;

    virtual TYPES type() const;

    static TYPES sType()
    {
        return SCREEN_MOVEMENT;
    }

    static const bool s_registered;

private:
    QString m_code;
    QString m_data;
};

#endif // SCREENMOVEMENT_HPP

#include "screenmovement.hpp"
#include "gameeventfactory.hpp"

ScreenMovement::ScreenMovement(const QVariantMap& data)
{
    init(data);

    QVariantMap::const_iterator i = data.find("code");
    Q_ASSERT(i != data.end());
    m_code =  i.value().toString();

    i = data.find("data");
    Q_ASSERT(i != data.end());
    m_data =  i.value().toString();
}

QString ScreenMovement::toString() const
{
    return "screen movement event: code = " + m_code + " and data = " + m_data;
}

ScreenMovement::TYPES ScreenMovement::type() const
{
    return sType();
}

namespace {
GameEvent* createScreenMovement(const QVariantMap& data)
{
    return new ScreenMovement(data);
}
}

const bool ScreenMovement::s_registered = GameEventFactory::instance().registerProduct(ScreenMovement::sType(), &createScreenMovement);

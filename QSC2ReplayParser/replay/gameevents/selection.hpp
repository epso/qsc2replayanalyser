#ifndef SELECTION_HPP
#define SELECTION_HPP

#include "gameevent.hpp"

class Selection : public GameEvent
{
public:
    class SelectionPair
    {
    public:
        SelectionPair(quint32 id, quint8 amount);

        quint32 id() const;
        quint8 amount() const;

    private:
        quint32 m_id;
        quint8 m_amount;
    };

    Selection(const QVariantMap& data);

    virtual QString toString() const;

    virtual TYPES type() const;

    static TYPES sType()
    {
        return SELECTION;
    }

    quint8 header() const;
    const QList<SelectionPair>& selections() const;
    const QList<quint32>& idsSelection() const;

    static const bool s_registered;

private:
    quint8 m_header;
    QList<SelectionPair> m_selections;
    QList<quint32> m_idsSelection;
};

#endif // SELECTION_HPP

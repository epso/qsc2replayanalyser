#include "gamestart.hpp"
#include "gameeventfactory.hpp"

GameStart::GameStart(const QVariantMap& data)
{
    init(data);
}

QString GameStart::toString() const
{
    return "game starts";
}

GameStart::TYPES GameStart::type() const
{
    return sType();
}

namespace {
GameEvent* createGameStart(const QVariantMap& data)
{
    return new GameStart(data);
}
}

const bool GameStart::s_registered = GameEventFactory::instance().registerProduct(GameStart::sType(), &createGameStart);

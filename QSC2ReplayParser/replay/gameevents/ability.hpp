#ifndef ABILITY_HPP
#define ABILITY_HPP

#include "gameevent.hpp"

class Ability : public GameEvent
{
public:
    Ability(const QVariantMap& data);

    virtual QString toString() const;

    virtual TYPES type() const;

    static TYPES sType()
    {
        return ABILITY;
    }

    static const bool s_registered;

private:
    QString m_comment;
};

#endif // ABILITY_HPP

#ifndef GAMEEVENTS_HPP
#define GAMEEVENTS_HPP

#include <QList>
#include <QVariantMap>

class GameEvent;
class StreamReader;

class GameEvents : public QList<GameEvent*>
{
public:
    GameEvents();
    ~GameEvents();

    static GameEvents* parse(StreamReader& flux);

private:
    void parseInitializationEvent(StreamReader& flux, QVariantMap& mapData);
    void parsePlayerActionEvent(StreamReader& flux, QVariantMap& mapData);
    void parseCameraMovementEvent(StreamReader& flux, QVariantMap& mapData);
};

#endif // GAMEEVENTS_HPP

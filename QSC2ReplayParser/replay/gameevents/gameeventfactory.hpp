#ifndef GAMEEVENTFACTORY_HPP
#define GAMEEVENTFACTORY_HPP

#include <utils/factory.hpp>
#include <replay/gameevents/gameevent.hpp>

class GameEventFactory : public Factory<GameEvent, GameEvent::TYPES, GameEvent*(*)(const QVariantMap&)>
{};
//{
//public:
//    static GameEvent* createGameEvent(GameEvent::TYPES type, const QVariantMap& data);

//    static bool registerProduct(GameEvent::TYPES type, GameEvent*(*callback)(const QVariantMap&));

//private:
//    typedef Factory<GameEvent, GameEvent::TYPES, GameEvent*(*)(const QVariantMap&)> type_factory;
//};

#endif // GAMEEVENTFACTORY_HPP

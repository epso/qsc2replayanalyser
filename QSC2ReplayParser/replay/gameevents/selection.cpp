#include "selection.hpp"
#include "gameeventfactory.hpp"

Selection::SelectionPair::SelectionPair(quint32 id, quint8 amount): m_id(id), m_amount(amount)
{}

quint32 Selection::SelectionPair::id() const
{
    return m_id;
}

quint8 Selection::SelectionPair::amount() const
{
    return m_amount;
}

Selection::Selection(const QVariantMap& data)
{
    init(data);

    QVariantMap::const_iterator i = data.find("header");
    Q_ASSERT(i != data.end());
    m_header = (quint8)i.value().toUInt();
    i = data.find("selectionList");
    Q_ASSERT(i != data.end());
    QVariantList lst = i.value().toList();
    uint total = 0;
    for(int i = 0; i < lst.size(); ++i)
    {
        QVariantMap map = lst.at(i).toMap();
        quint8 nb = (quint8)(map["nb"].toUInt());
        total += nb;
        m_selections << SelectionPair((quint32)(map["id"].toULongLong()), nb);
    }
    i = data.find("idList");
    Q_ASSERT(i != data.end());
    lst = i.value().toList();
    for(int i = 0; i < lst.size(); ++i)
        m_idsSelection << (quint32)(lst.at(i).toULongLong());
    Q_ASSERT((uint)m_idsSelection.size() == total);
}

QString Selection::toString() const
{
    return "selection event";
}

Selection::TYPES Selection::type() const
{
    return sType();
}

quint8 Selection::header() const
{
    return m_header;
}

const QList<Selection::SelectionPair>& Selection::selections() const
{
    return m_selections;
}

const QList<quint32>& Selection::idsSelection() const
{
    return m_idsSelection;
}

namespace {
GameEvent* createSelection(const QVariantMap& data)
{
    return new Selection(data);
}
}

const bool Selection::s_registered = GameEventFactory::instance().registerProduct(Selection::sType(), &createSelection);

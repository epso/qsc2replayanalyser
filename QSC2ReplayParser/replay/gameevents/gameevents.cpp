#include "gameevents.hpp"
#include "gameeventfactory.hpp"
#include <utils/bitarray.hpp>
#include <parser.hpp>
#include <replay/parsingerror.hpp>
#include <streamreader.hpp>

#include <cassert>

#include <QDataStream>
#include <QTime>

GameEvents::GameEvents()
{
}

GameEvents::~GameEvents()
{
    qDeleteAll(*this);
}

#include <QDebug>

GameEvents* GameEvents::parse(StreamReader& flux)
{
    qDebug() << "GameEvents::parse()";
    GameEvents* res = new GameEvents;
    quint8 i;

    quint64 timestamp;
    while(!flux.atEnd())
    {
        QVariantMap mapData;
        qDebug() << "\nparsing new event";
        flux >> i;
        BitArray ba(i);
        qDebug() << BitArray::toHex(i) <<  " => " << ba.toString();
        ba.pop_back();
        ba.pop_back();
        if((i&3) > 0)
        {
            quint8 n = i&3;
            for(quint8 cpt = 0; cpt < n; ++cpt)
            {
                flux >> i;
                qDebug() << "rajout d'un octet au timestamp: " << BitArray::toHex(i);
                ba.push_back(BitArray(i));
            }
        }
        timestamp = ba.toLong();
        QTime time;
        time = time.addMSecs(((double)timestamp / 16)*1000);
        qDebug() << "timestamp final:" << ba.toHex() << " => " << time.toString("H:m:s:zzz");
        mapData["timestamp"] = timestamp;

        flux >> i;
        quint8 player = (i&31); //i&7
        quint8 eventType = i>>5;
        mapData["player"] = (uint)player;
        qDebug() << BitArray::toHex(i) <<  " => player = " << player << " et type = " << eventType;
        if(eventType == 0) res->parseInitializationEvent(flux, mapData);
        else if(eventType == 1) res->parsePlayerActionEvent(flux, mapData);
        else if(eventType == 3) res->parseCameraMovementEvent(flux, mapData);
        else if(eventType == 2) {
            qDebug() << "parsing type 2 event:";
            BitArray b;
            for(uint n = 0; n < 10; ++n) //lecture de 10octets
            {
                flux >> i;
                b.push_back(BitArray(i));
            }
            qDebug() << b.toHex() << " (" << b.size() << ")" << endl;
            mapData["data"] = b.toHex();
            *res << GameEventFactory::instance().create(GameEvent::TYPE_TWO, mapData);

        }
        else throw ParsingError("Type inconnu");
    }

    return res;
}

void GameEvents::parseInitializationEvent(StreamReader& flux, QVariantMap& mapData)
{
    quint8 i;
    uint player = mapData["player"].toUInt();
    flux >> i;
    if(i == 0x2C || i == 0x0C || i == 0x0B) //player enter event
    {
        if(player >= 15) throw ParsingError("erreur, player enter event with invalid id code" + QString::number(player));
        qDebug() << "player #" + QString::number(player) + " entering";
        *this << GameEventFactory::instance().create(GameEvent::PLAYER_ENTER, mapData);
        return;
    }
    if(i == 5) //game start event
    {
        qDebug() << "game start";
        if(player != 16) throw ParsingError("game start event with non-universal player code");
        *this << GameEventFactory::instance().create(GameEvent::GAME_START, mapData);
        return;
    }

    throw ParsingError("unknown initialisation event: " + BitArray::toHex(i));
}

void GameEvents::parsePlayerActionEvent(StreamReader& flux, QVariantMap& mapData)
{
    quint8 i;
    flux >> i;
    qDebug() << "player action event: " << BitArray::toHex(i);
    if(i == 9)
    {
        qDebug() << "player #" << mapData["player"].toUInt() << "has left the game";
        *this << GameEventFactory::instance().create(GameEvent::PLAYER_LEFT, mapData);
        return;
    }
    if((i&15)==12)
    {
        qDebug() << "player selection event";
        flux >> i;
        if(i != 0) qDebug() << "header byte is " << BitArray::toHex(i);
        mapData["header"] = (uint)i;
        flux >> i;
        quint8 deselectionType = i&3;
        qDebug() << "deselection type: " << deselectionType;
        //i = i>>2;
        qDebug() << "deselect unit: " << BitArray::toHex(i);
        quint8 nbUnitType;
        if(i != 0)
        {
            quint8 cpt = i;
            BitArray b;
            while(cpt >= 8)
            {
                flux >> i;
                qDebug() << "ajout de " << BitArray::toHex(i);
                b.push_back(BitArray(i));
                cpt -= 8;
            }
            if(cpt > 0)
            {
                assert(cpt < 8);
                qDebug() << "starting" << cpt << "bits bitshift";
                b.push_back(flux.startBitShift(cpt));
                qDebug() << "map = " << b.toString() << " (" << b.toHex() << ")";
                qDebug() << "lecture des " << cpt << "derniers bits de la map contenus dans " << BitArray::toHex(i);

            }
        }
        flux >> nbUnitType;
        //handling selection 
        qDebug() << nbUnitType << "different unit type selected";
        quint32 nbUnit = 0;
        QVariantList lst;
        for(quint8 cpt = 0; cpt < nbUnitType; cpt++)
        {
            BitArray b = flux.readTypeID();
            flux >> i;
            nbUnit += i;
            qDebug() << "id = " << b.toString() << " => " << b.toHex();
            QVariantMap map;
            map["id"] = b.toLong();
            map["nb"] = (uint)i;
            lst.push_back(map);
        }
        mapData["selectionList"] = lst;
        if(nbUnit != i) throw ParsingError("number of units selected doesn't match: " + QString::number(nbUnit) + " != " + QString::number(i));
        lst.clear();
        for(quint32 cpt = 0; cpt < nbUnit; ++cpt)
        {
            BitArray b = flux.readNBytes(4);
            qDebug() << "unit #" << cpt << ": " << b.toString() << " => " << b.toHex();
            lst.push_back(b.toLong());
        }
        if(!flux.endBitShift()) qDebug() << "Error while closing bitshift!!";
        else qDebug() << "bitshift closed properly";
        mapData["idList"] = lst;
        *this << GameEventFactory::instance().create(GameEvent::SELECTION, mapData);
        return;
    }
    if((i&15)==11) //unit ability event
    {
        qDebug() << "player unit ability event";
        quint8 flag, type, code;
        flux >> flag;
        qDebug() << "flag:" << BitArray::toHex(flag);

        flux >> type;
        qDebug() << BitArray::toHex(type);
        bool hasTargetUnit = (type & 0x80)>0;
        bool hasTargetLocation = (type & 0x40)>0;
        bool isCommandCard = (type & 0x20)>0;
        bool isQueued = (type & 2)>0;

        qDebug() <<  (hasTargetUnit?"hasTargetUnit":"") << " | " << (hasTargetLocation?"hasTargetLocation":"") << " | " << (isCommandCard?"isCommandCard":"") << " | " << (isQueued?"isQueued":"");

        flux >> i;
        BitArray b(i);
        qDebug() << BitArray::toHex(i);
        flux >> i;
        b.push_back(i);
        qDebug() << BitArray::toHex(i);
        flux >> i;
        b.push_back(i);
        qDebug() << BitArray::toHex(i);

        quint64 l = 0x00012100;
        if(b.toLong() == l)
        {
            qDebug() << "Creation d'une probe!!!!";
            mapData["comment"] = "probe creation";
            *this << GameEventFactory::instance().create(GameEvent::ABILITY, mapData);
            return;
        }
        l = 0x00360100;
        if(b.toLong() == l) {
            flux >> i;
            BitArray b(i);
            flux >> i;
            b.push_back(BitArray(i));
            flux >> i;
            b.push_back(BitArray(i));
            qDebug() << "Go au minerai!";
            qDebug() << "reste:" << b.toHex();
            mapData["comment"] = "go to mineral";
            *this << GameEventFactory::instance().create(GameEvent::ABILITY, mapData);
            return;
        }
        l = 0x008DF57D;
        if(b.toLong() == l) {
            qDebug() << "unknown player ability (movement order?)";
            b = BitArray();
            for(int n = 0; n < 7; ++n)
            {
                flux >> i;
                b.push_back(BitArray(i));
            }
            qDebug() << "data: " << b.toHex();
            mapData["comment"] = "maybe movement order";
            *this << GameEventFactory::instance().create(GameEvent::ABILITY, mapData);
            return;
        }

        //throw ParsingError("ability a finir");
        b = BitArray();
        for(int n = 0; n < 6; ++n)
        {
            flux >> i;
            b.push_back(BitArray(i));
        }
        qDebug() << "reste: " << b.toHex();
        return;

    }
    throw ParsingError("unknown player action event: " + BitArray::toHex(i));
}

void GameEvents::parseCameraMovementEvent(StreamReader& flux, QVariantMap& mapData)
{
    quint8 i;
    flux >> i;
    qDebug() << "camera event: " << BitArray::toHex(i);
    if((i&15) == 1)
    {
        QString str;
        for(int cpt =0; cpt < 10; cpt++)  //reading ten bytes
        {
            flux >> i;
            str += BitArray::toHex(i)+" ";
        }
        qDebug() << "code X1 => screen movement " << str;
        mapData["code"] = "x1";
        mapData["data"] = str;
        *this << GameEventFactory::instance().create(GameEvent::SCREEN_MOVEMENT, mapData);
        return;
    }
    throw ParsingError("unknown Camera Movement event: " + BitArray::toHex(i));
}

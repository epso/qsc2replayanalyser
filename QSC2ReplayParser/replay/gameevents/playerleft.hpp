#ifndef PLAYERLEFT_HPP
#define PLAYERLEFT_HPP

#include "gameevent.hpp"

class PlayerLeft : public GameEvent
{
public:
    PlayerLeft(const QVariantMap& data);

    virtual QString toString() const;

    virtual TYPES type() const;

    static TYPES sType()
    {
        return PLAYER_LEFT;
    }

    static const bool s_registered;
};

#endif // PLAYERLEFT_HPP

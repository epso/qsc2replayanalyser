#ifndef TYPETWO_HPP
#define TYPETWO_HPP

#include "gameevent.hpp"

class TypeTwo : public GameEvent
{
public:
    TypeTwo(const QVariantMap& data);

    virtual QString toString() const;

    virtual TYPES type() const;

    static TYPES sType()
    {
        return TYPE_TWO;
    }

    static const bool s_registered;

private:
    QString m_data;
};

#endif // TYPETWO_HPP

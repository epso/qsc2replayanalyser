#ifndef GAMESTART_HPP
#define GAMESTART_HPP

#include "gameevent.hpp"

class GameStart : public GameEvent
{
public:
    GameStart(const QVariantMap& data);

    virtual QString toString() const;

    virtual TYPES type() const;

    static TYPES sType()
    {
        return GAME_START;
    }

    static const bool s_registered;
};

#endif // GAMESTART_HPP

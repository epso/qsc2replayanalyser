#include "playerenter.hpp"
#include "gameeventfactory.hpp"

PlayerEnter::PlayerEnter(const QVariantMap& data)
{
    init(data);
}

QString PlayerEnter::toString() const
{
    return "player #" + QString::number(player()) + " enters the game";
}

PlayerEnter::TYPES PlayerEnter::type() const
{
    return sType();
}

namespace {
GameEvent* createPlayerEnter(const QVariantMap& data)
{
    return new PlayerEnter(data);
}
}

const bool PlayerEnter::s_registered = GameEventFactory::instance().registerProduct(PlayerEnter::sType(), &createPlayerEnter);

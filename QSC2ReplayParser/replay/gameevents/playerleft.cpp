#include "playerleft.hpp"
#include "gameeventfactory.hpp"

PlayerLeft::PlayerLeft(const QVariantMap& data)
{
    init(data);
}

QString PlayerLeft::toString() const
{
    return "player #" + QString::number(player()) + " left the game";
}

PlayerLeft::TYPES PlayerLeft::type() const
{
    return sType();
}

namespace {
GameEvent* createPlayerLeft(const QVariantMap& data)
{
    return new PlayerLeft(data);
}
}

const bool PlayerLeft::s_registered = GameEventFactory::instance().registerProduct(PlayerLeft::sType(), &createPlayerLeft);

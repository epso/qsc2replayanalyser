#include "gameevent.hpp"

GameEvent::GameEvent()
{}

quint64 GameEvent::timestamp() const
{
    return m_timestamp;
}

uint GameEvent::player() const
{
    return m_player;
}

void GameEvent::init(const QVariantMap& data)
{
    m_timestamp = data["timestamp"].toULongLong();
    m_player = data["player"].toUInt();
}

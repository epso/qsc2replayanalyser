#include "ability.hpp"
#include "gameeventfactory.hpp"

Ability::Ability(const QVariantMap& data)
{
    init(data);

    QVariantMap::const_iterator i = data.find("comment");
    Q_ASSERT(i != data.end());
    m_comment =  i.value().toString();
}

QString Ability::toString() const
{
    return "ability event: " + m_comment;
}

Ability::TYPES Ability::type() const
{
    return sType();
}

namespace {
GameEvent* createAbility(const QVariantMap& data)
{
    return new Ability(data);
}
}

const bool Ability::s_registered = GameEventFactory::instance().registerProduct(Ability::sType(), &createAbility);

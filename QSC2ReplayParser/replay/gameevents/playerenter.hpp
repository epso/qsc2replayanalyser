#ifndef PLAYERENTER_HPP
#define PLAYERENTER_HPP

#include "gameevent.hpp"

class PlayerEnter : public GameEvent
{
public:
    PlayerEnter(const QVariantMap& data);

    virtual QString toString() const;

    virtual TYPES type() const;

    static TYPES sType()
    {
        return PLAYER_ENTER;
    }

    static const bool s_registered;
};

#endif // PLAYERENTER_HPP

#ifndef PARSINGERROR_HPP
#define PARSINGERROR_HPP

#include <stdexcept>

#include <QString>

class ParsingError : public std::exception
{
public:
    ParsingError(const QString& message);
    ~ParsingError() throw();

    const char* what() const throw();

private:
    QString m_message;
};

#endif // PARSINGERROR_HPP

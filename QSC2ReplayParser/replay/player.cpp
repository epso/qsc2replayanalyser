#include "player.hpp"

#include <stdexcept>

Player::Player()
{
}

Player* Player::parse(const QVariantMap& map)
{
    Player* res = new Player;
    res->m_nom = map.value("0").toString();
    res->m_id = map.value("0").toMap().value("4").toLongLong();
    res->setRace(map.value("2").toString());
    QVariantMap couleur = map.value("3").toMap();
    res->m_couleur = QColor(couleur.value("1").toLongLong(), couleur.value("2").toLongLong(), couleur.value("3").toLongLong(), couleur.value("0").toLongLong());
    res->m_equipe = map.value("5").toLongLong();
    res->m_handicap = map.value("6").toLongLong();
    res->m_resultat = map.value("8").toLongLong();
    return res;
}

QString Player::nom() const
{
    return m_nom;
}

Player::Race Player::race() const
{
    return m_race;
}

QColor Player::couleur() const
{
    return m_couleur;
}

quint8 Player::equipe() const
{
    return m_equipe;
}

quint8 Player::handicap() const
{
    return m_handicap;
}

quint8 Player::resultat() const
{
    return m_resultat;
}

qlonglong Player::id() const
{
    return m_id;
}

void Player::setRace(QString race) throw(std::invalid_argument)
{
    if(race=="Protoss") m_race = Protoss;
    else if(race=="Zerg") m_race = Zerg;
    else if(race=="Terran") m_race = Terran;
    else throw std::invalid_argument(("`" + race + "` isn't a valid race.").toStdString());
}

bool Player::gagne() const
{
    return m_resultat == 1;
}

bool Player::perdu() const
{
    return m_resultat == 2;
}

QString Player::race2String(Race race) throw(std::invalid_argument)
{
    switch(race)
    {
        case Protoss: return "Protoss";
        case Zerg: return "Zerg";
        case Terran: return "Terran";
        default:
            throw std::invalid_argument((QString::number((int)race) + " isn't a valid race.").toStdString());
    }
}

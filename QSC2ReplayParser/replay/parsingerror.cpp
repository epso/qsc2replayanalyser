#include "parsingerror.hpp"

ParsingError::ParsingError(const QString& message): m_message(message)
{}

ParsingError::~ParsingError() throw()
{}

const char* ParsingError::what() const throw()
{
    return m_message.toStdString().c_str();
}

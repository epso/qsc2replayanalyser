#ifndef PLAYER_H
#define PLAYER_H

#include <stdexcept>

#include <QString>
#include <QColor>
#include <QVariantMap>

class Player
{
public:
    enum Race {Protoss, Terran, Zerg};

    Player();

    static Player* parse(const QVariantMap& map);

    QString nom() const;
    Race race() const;
    QColor couleur() const;
    quint8 equipe() const;
    quint8 handicap() const;
    quint8 resultat() const;
    qlonglong id() const;

    void setRace(QString race) throw(std::invalid_argument);

    bool gagne() const;
    bool perdu() const;

    static QString race2String(Race race) throw(std::invalid_argument);

private:
    QString m_nom;
    Race m_race;
    QColor m_couleur;
    quint8 m_equipe;
    quint8 m_handicap;
    quint8 m_resultat; //(1 = win, 2 = loss, 0 = unknown)
    qlonglong m_id;
};

#endif // PLAYER_H

#include "replay.hpp"
#include "player.hpp"
#include "gameevents/gameevents.hpp"

Replay::Replay(): m_gameEvents(0)
{
}

Replay::~Replay()
{
    qDeleteAll(m_joueurs);
}

#include <QDebug>

Replay* Replay::parse(const QVariantMap& map)
{
    Replay* res = new Replay;
    QVariantList joueurs = map.value("0").toList();
    for(QVariantList::const_iterator i = joueurs.begin(); i != joueurs.end(); ++i)
    {
        Player* p = Player::parse(i->toMap());
        res->m_joueurs.push_back(p);
        qDebug() << "adding player " << p->id() << " => " << p->nom() << " (" << Player::race2String(p->race()) << ")";
    }
    res->m_map = map.value("1").toString();
    res->m_datetime = map.value("5").toLongLong();
    res->m_timezoneOffset = map.value("6").toLongLong();
    return res;
}

QString Replay::map() const
{
    return m_map;
}

const Replay::type_liste_players& Replay::joueurs() const
{
    return m_joueurs;
}

qlonglong Replay::datetime() const
{
    return m_datetime;
}

qlonglong Replay::timezoneOffset() const
{
    return m_timezoneOffset;
}

const GameEvents* Replay::gameEvents() const
{
    return m_gameEvents;
}

void Replay::setGameEvents(GameEvents* gevts)
{
    if(m_gameEvents) delete m_gameEvents;
    m_gameEvents = gevts;
}

#ifndef ENTETE_HPP
#define ENTETE_HPP

#include <QVariantMap>

class EnTete
{
public:
    EnTete();

    void load(const QVariantMap& map);

    QString enTeteString() const;
    qlonglong majorVersion() const;
    qlonglong minorVersion() const;
    qlonglong patchNumber() const;
    qlonglong buildNumber() const;
    qlonglong gameLength() const;
    double gameDuration() const; //temps de jeu en secondes

    QString toJson() const;

private:
    QString m_enTeteString; //Chaine de charactere en debut du fichier
    qlonglong m_majorVersion; //major version
    qlonglong m_minorVersion; //minor version
    qlonglong m_patchNumber; //patch number
    qlonglong m_buildNumber; //Build Number
    qlonglong m_gameLength; //game length (in frames)
};

#endif // ENTETE_HPP

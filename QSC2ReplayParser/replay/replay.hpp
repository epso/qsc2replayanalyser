#ifndef REPLAY_H
#define REPLAY_H

#include <QString>
#include <QVariantMap>

class Player;
class GameEvents;

class Replay
{
public:
    typedef QList<Player*> type_liste_players;

    Replay();

    virtual ~Replay();

    static Replay* parse(const QVariantMap& map);

    QString map() const;
    const type_liste_players& joueurs() const;
    qlonglong datetime() const;
    qlonglong timezoneOffset() const;

    const GameEvents* gameEvents() const;
    void setGameEvents(GameEvents* gevts);

private:
    QString m_map;
    type_liste_players m_joueurs;
    qlonglong m_datetime;
    qlonglong m_timezoneOffset;
    GameEvents* m_gameEvents;
};

#endif // REPLAY_H

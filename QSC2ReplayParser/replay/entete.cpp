#include "entete.hpp"

EnTete::EnTete()
{}

void EnTete::load(const QVariantMap& map)
{
    m_enTeteString = map.value("0").toString();
    QVariantMap m = map.value("1").toMap();

    m_majorVersion = m.value("1").toLongLong();
    m_minorVersion = m.value("2").toLongLong();
    m_patchNumber = m.value("3").toLongLong();
    m_buildNumber = m.value("4").toLongLong();

    m_gameLength = map.value("3").toLongLong();
}

QString EnTete::enTeteString() const
{
    return m_enTeteString;
}

qlonglong EnTete::majorVersion() const
{
    return m_majorVersion;
}

qlonglong EnTete::minorVersion() const
{
    return m_minorVersion;
}

qlonglong EnTete::patchNumber() const
{
    return m_patchNumber;
}

qlonglong EnTete::buildNumber() const
{
    return m_buildNumber;
}

qlonglong EnTete::gameLength() const
{
    return m_gameLength;
}

double EnTete::gameDuration() const
{
    return ((double)m_gameLength)/16;
}

QString EnTete::toJson() const
{
    QString res;
    res += m_enTeteString + "\n";
    res += "major version: " + QString::number(m_majorVersion) + "\n";
    res += "minor version: " + QString::number(m_minorVersion) + "\n";
    res += "patch number: " + QString::number(m_patchNumber) + "\n";
    res += "build number: " + QString::number(m_buildNumber) + "\n";
    res += "game length: " + QString::number(gameDuration()) + "s\n";
    return res;
}

#include "parser.hpp"
#include "mpqarchive.hpp"
#include <utils/bitarray.hpp>
#include "mpqfile.hpp"
#include <utils/json.hpp>
#include <replay/replay.hpp>
#include <replay/gameevents/gameevents.hpp>
#include <streamreader.hpp>

#include <cassert>
#include <stdexcept>
#include <cmath>

#include <QVariant>
#include <QTime>

#include <QDebug>

quint8 Parser::start[4] = {'M','P','Q',27};

Parser::Parser(QObject *parent) :
    QObject(parent), m_replay(0)
{}

void Parser::loadReplay(const QString& filename)
{
    parseHeader(filename);

    try {
        MPQArchive::type_ptr archive = MPQArchive::openArchive(filename.toStdString());
        emit message("Archive ouverte");

        //        const MPQFile& infoFile    = archive->openFile("replay.info");
        //        const MPQFile& gameFile    = archive->openFile("replay.game.events");
        //        const MPQFile& syncFile    = archive->openFile("replay.sync.events");
        //        const MPQFile& messageFile = archive->openFile("replay.message.events");
        //        const MPQFile& saveFile    = archive->openFile("save.jpg");

        const MPQFile& detailsFile = archive->openFile("replay.details");
        const MPQFile& gameEventsFile = archive->openFile("replay.game.events");

        emit message("fichiers ouverts");

        parseDetailsFile(detailsFile);
        parseGameEventsFile(gameEventsFile);

//        files:
//        replay.details (basic metadata)
//        replay.initData (unknown)
//        replay.attribute.events (unknown)
//        replay.game.events (actions)
//        replay.message.events (chat, ping)
//        replay.smartcam.events (presumably player cameras)
//        replay.sync.events (presumably consistency checks)
    }
    catch(std::exception& ex)
    {
        emit erreur(ex.what());
    }
    emit message("Chargement terminé");
}

void Parser::parseDetailsFile(const MPQFile& detailsFile)
{
    QByteArray byteArray;
    QDataStream flux(&byteArray, QIODevice::ReadWrite);
    bool res = detailsFile.toDataStream(flux);
    if(!res)
    {
        emit erreur("Impossible de charger le datastream de detailsFile");
        return;
    }

    flux.device()->seek(0);
    QVariant data = parse(flux);
    if(m_replay) delete m_replay;
    m_replay = Replay::parse(data.toMap());
    assert(m_replay);
}

#include <QFile>

void Parser::parseHeader(const QString& file)
{
    qDebug() << file;
    QFile f(file);
    if(!f.exists())
    {
        emit message("Impossible de charger le fichier");
        return;
    }
    f.open(QIODevice::ReadOnly);
    QDataStream flux(&f);

    //lecture de l'en tete
    quint8 i;
    for(quint8 n = 0; n < 4; n++)
    {
        flux >> i;
        if(i != start[n]) {
            emit message("Format de fichier invalide ("+QString::number(n)+")");
            return;
        }
    }

    qint32 j;
    flux >> j;
    //qDebug() << "HeaderSize: " << j;
    flux >> j;
    //qDebug() << "ArchiveSize: " << j;
    flux >> j;
    //qDebug() << "Rest: " << j;

    QVariant t = parse(flux);
    m_enTete.load(t.toMap());

    qDebug() << "Build Number: " << m_enTete.buildNumber();
    qDebug() << "Patch Number: " << m_enTete.patchNumber();
    qDebug() << "Game length: " <<  m_enTete.gameDuration();
    QTime time;
    time = time.addMSecs(m_enTete.gameDuration()*1000);
    qDebug() << "Game length: " <<  time.toString("H:m:s:zzz");

    f.close();
}

void Parser::parseGameEventsFile(const MPQFile& gameEventsFile)
{
    StreamReader flux;
    bool res = gameEventsFile.toStreamReader(flux);
    if(!res)
    {
        emit erreur("Impossible de charger le datastream de detailsFile");
        return;
    }


    /*
    * just to have a text output for checking
    */
    QFile sortie("replay.game.event.txt");
    if(!sortie.open(QIODevice::WriteOnly))
    {
        qDebug() << "Impossible d'ouvrir le fichier";
        return;
    }
    QTextStream out(&sortie);
    quint8 i;
    quint8 cpt = 0;
    QString str;
    flux.seek(0);
    while(!flux.atEnd())
    {
        flux >> i;
        str += BitArray::toHex(i) + " ";
        if(cpt++ > 8) {
            cpt = 0;
            str += "\n";
            out << str;
            str = "";
        }
    }
    out << str;
    out << "\n";

    qDebug() << "parsing game events:";
    flux.seek(0);
    GameEvents* gameEvents = GameEvents::parse(flux);
    if(m_replay) m_replay->setGameEvents(gameEvents);
    else delete gameEvents;
}

QVariant Parser::parse(QDataStream& flux) const
{
    quint8 type, tmp;
    flux >> type;
    while(type == 0) flux >> type;
    if(type == 2)
    {
        quint8 taille;
        flux >> taille;
        taille /= 2;
        QString res;
        for(quint8 i = 0; i < taille; ++i)
        {
            flux >> tmp;
            res += QChar(tmp);
        }
        return QVariant(res);
    }
    if(type == 4)
    {
        flux >> tmp;
        Q_ASSERT(tmp == 1);
        flux >> tmp;
        Q_ASSERT(tmp == 0);
        flux >> tmp;
        quint8 nbElements = parseSingleByte(tmp);
        //qDebug() << nbElements << " élements dans le tableau";
        QVariantList lst;
        for(quint8 n = 0; n < nbElements; ++n)
            lst.push_back(parse(flux));
        Q_ASSERT(nbElements == lst.size());
        return lst;
    }
    if(type == 5)
    {
        QVariantMap map;
        quint8 nbPaires, index;
        //qDebug() << "parsing a Key-Value Object";
        flux >> nbPaires;
        nbPaires /= 2;
        //qDebug() << nbPaires << " paires";
        for(quint8 n = 0; n < nbPaires; ++n)
        {
            flux >> index;
            map[QString::number(index/2)] = parse(flux);

        }
        Q_ASSERT(nbPaires == map.size());
        return QVariant(map);
    }
    if(type == 6)
    {
        flux >> tmp;
        return QVariant(parseSingleByte(tmp));
    }
    if(type == 7)
    {
        quint8 a,b,c,d;
        flux >> a;
        flux >> b;
        flux >> c;
        flux >> d;
        //qDebug() << a << " " << b << " " << c << " " << d;
        qlonglong res = ((qlonglong)a)*256*256*256 + ((qlonglong)b)*256*256 + ((qlonglong)c)*256 + d;
        if(res%2 == 1) res = -res/2;
        else res /=2;
        //qDebug() << res;
        return QVariant(res);
    }
    if(type == 9)
    {
        BitArray array;
        flux >> tmp;
        //qDebug() << "parsing a Variable Length Format (VLF) Integer: " << tmp;

        //qlonglong res = 0;
        if((tmp & 0x80) > 0)
        {
            while((tmp & 0x80) > 0) {
                BitArray a;
                for(qint8 i = 6; i >=0; --i)
                    a.QVector<bool>::push_back((tmp&(1<<i))>0);
                array.push_front(a);
                flux >> tmp;
            }
        }
        BitArray a;
        for(qint8 i = 6; i >=0; --i)
            a.QVector<bool>::push_back((tmp&(1<<i))>0);

        array.push_front(a);
        bool neg = array.back();
        array.pop_back();
        if(neg) {
            //qDebug() << -array.toLong();
            return QVariant(-array.toLong());
        }
        else {
            //qDebug() << array.toLong();
            return QVariant(array.toLong());
        }
    }
    qDebug() << "index pas géré:" << BitArray::toHex(type);
    return QVariant();
}

qint8 Parser::parseSingleByte(quint8 i)
{
    return (i%2==0?i/2:-i/2);
}

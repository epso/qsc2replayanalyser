#ifndef MPQFILE_HPP
#define MPQFILE_HPP

#include <string>

class StreamReader;

class QDataStream;

class MPQFile
{
public:
    MPQFile(const std::string& filename, unsigned char* buffer, unsigned int size);
    ~MPQFile();

    const std::string& filename() const;

    unsigned int size() const;

    const unsigned char* buffer() const;

    bool toDataStream(QDataStream& stream) const;

    bool toStreamReader(StreamReader& stream) const;

private:
    MPQFile(const MPQFile& file);

    std::string m_filename;
    unsigned char* m_buffer;
    unsigned int m_size;
};

#endif // MPQFILE_HPP

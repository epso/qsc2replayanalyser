#-------------------------------------------------
#
# Project created by QtCreator 2012-07-14T20:41:02
#
#-------------------------------------------------

QT       += core gui

include(../QSC2ReplayAnalyser.pri)

TARGET = $$SC2REPLAYANALYSER_BIN/QSC2ReplayAnalyser
TEMPLATE = app

OBJECTS_DIR = $$QSC2REPLAYANALYSER_OBJ
MOC_DIR = $$QSC2REPLAYANALYSER_OBJ

INCLUDEPATH += .

LIBS += -L$$QSC2REPLAYANALYSER_BIN
LIBS += -lStorm -lbz2 -lz -lQSC2ReplayParser

win32 {
    LIBS +=  -lwininet
}

SOURCES += main.cpp\
        mainwindow.cpp 

HEADERS  += \
    mainwindow.hpp 


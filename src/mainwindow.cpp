#include "mainwindow.hpp"
#include <Parser>
#include <MPQFile>
#include <utils/BitArray>

#include <QAction>
#include <QFileDialog>
#include <QMenuBar>
#include <QMenu>
#include <QInputDialog>

#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QAction* ouvrir = new QAction("ouvrir un replay", this);
    ouvrir->setShortcut(tr("Ctrl+O"));
    connect(ouvrir, SIGNAL(triggered()), this, SLOT(ouvrirFichier()));

    QAction* parse = new QAction("parser une chaine", this);
    parse->setShortcut(tr("Ctrl+E"));
    connect(parse, SIGNAL(triggered()), this, SLOT(parserString()));

    QMenu* m = menuBar()->addMenu("&Fichiers");
    m->addAction(ouvrir);
    m->addSeparator();
    m->addAction(parse);

    m_parser = new Parser(this);
    connect(m_parser, SIGNAL(message(QString)), this, SLOT(onParserMessage(QString)));
    connect(m_parser, SIGNAL(erreur(QString)), this, SLOT(onParserErreur(QString)));
}

MainWindow::~MainWindow()
{}

void MainWindow::ouvrirFichier()
{
    QString file = QFileDialog::getOpenFileName(this, tr("Ouvrir un replay"), QString(), tr("SC2 Replay (*.SC2Replay)"));
    if(file.isEmpty()) return;
    qDebug() << "ouverture de: " << file;
    if(!QFile::exists(file))
    {
        qDebug() << "le fichier " << file << " n'existe pas";
        return;
    }

    m_parser->loadReplay(file);
}

void MainWindow::onParserMessage(const QString& message)
{
    qDebug() << "INFO: " << message;
}

void MainWindow::onParserErreur(const QString& erreur)
{
    qDebug() << "ERREUR: " << erreur;
}

void MainWindow::parserString()
{
    QString str = QInputDialog::getText(this, tr("String to parse"), tr("String to parse (in hex)"));
    if(str.isEmpty()) return;
    str.replace(' ', "");
    str.replace('\n', "");
    str.replace('\t', "");
    qDebug() << str << "=>" << str.size();
    unsigned char* buffer = new unsigned char[str.size()/2];
    unsigned char c;
    QString tmp;
    for(int i = 0; i < str.size(); ++i)
    {
        tmp = str.at(i++);
        tmp += str.at(i);
        c = BitArray::fromHex(tmp);
        buffer[i/2] = c;
    }
    MPQFile f("test", buffer, str.size()/2);
    try
    {
        m_parser->parseGameEventsFile(f);
    }
    catch(std::exception& e)
    {
        qDebug() << "Exception: " << QString(e.what());
    }
}

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>

class Parser;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void ouvrirFichier();
    void onParserMessage(const QString& message);
    void onParserErreur(const QString& erreur);
    void parserString();

private:
    Parser* m_parser;
};

#endif // MAINWINDOW_H
